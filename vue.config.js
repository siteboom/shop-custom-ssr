const proxy = {
	'/vue-advanced-api': {
		target: 'http://faceprog.ru',
		secure: false,
		changeOrigin: true
	}
};

const isServer = process.argv.includes("--server");

const webpackNodeExternals = require("webpack-node-externals");

console.log(isServer ? "on server" : "on client");

const platformChainWebpack = (config) => {
	if (isServer) {
		config.plugins.delete("html");
		config.plugins.delete("preload");
		config.plugins.delete("prefetch");
	} else {
		config.plugin("html").tap((args) => {
			args[0].minify = false;
			return args;
		});
	}
};

const chainWebpack = (config) => {
	config.plugin("define").tap((options) => {
		options[0]["process.isClient"] = !isServer;
		options[0]["process.isServer"] = isServer;

		return options;
	});

	platformChainWebpack(config);
};

const configureWebpack = isServer
	? {
		target: "node",
		entry: { app: "./src/entry-server.js" },
		output: {
			//library: "someLibName",
			libraryTarget: "commonjs2",
			libraryExport: "default",
			filename: "js/server-bundle.js",
			//auxiliaryComment: "Test Comment",
		},
		optimization: {
			splitChunks: false,
		},
		externals: [webpackNodeExternals()],
	}
	: {
		entry: { app: "./src/entry-client.js" },
		devServer: {
			proxy,
			//https: true
		}
	};

module.exports = {
	filenameHashing: false,
	productionSourceMap: false,
	configureWebpack,
	chainWebpack
}