const path = require("path");
const express = require("express");
const fs = require("fs");
const server = express();
const initServerApp = require("./dist/js/server-bundle.js");
const template = fs.readFileSync("./dist/index.html", "utf-8");

const { renderToString } = require("@vue/server-renderer");

const {isSSRPage} = require("./src/utils/ssr")

server.use("/css", express.static(path.resolve(__dirname, "./dist/css")));
server.use("/js", express.static(path.resolve(__dirname, "./dist/js")));
server.use("/img", express.static(path.resolve(__dirname, "./dist/img")));
server.use(
    "/favicon.ico",
    express.static(path.resolve(__dirname, "./dist/favicon.ico"))
);

server.get("*", async (request, response) => {
    const context = {
        url: request.url,
    };

    const {app, store, router} = await initServerApp(context);

    let page

    renderToString(app).then(
        (html) => {
            if(isSSRPage(router)){
                 page = template
                    .replace("<!--ssr-here-->", html)
                    .replace('<!--ssr-store-here-->', `
                    <script>window.INITIAL_STATE = ${JSON.stringify(store.state)}</script>
                `)
            }else{
                page = template
            }

            response.end(page);
        },
        (err) => {
            console.log(err);
        }
    );
});

server.listen(3000);
console.log("starting...");