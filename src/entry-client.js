import initApp from './app'
import 'bootstrap/dist/css/bootstrap.css';

(async () => {
    const {app, store, router}  = await initApp()

    if(window.hasOwnProperty('INITIAL_STATE')){

        store.replaceState(window.INITIAL_STATE)



    }

    router.isReady().then(() => {

        const asyncPromises = []

        let component

        router.currentRoute.value.matched.forEach(function(route){
            component = route.components.default

            if(Object(component).hasOwnProperty('asyncData')){
                const f = component.asyncData.bind(component)
                //asyncPromises.push(f())
            }
        })

        Promise.all(asyncPromises).then(() => {
            app.mount('#app');
        }).catch(() => {
            console.log('catched err')
        })

    })
})()