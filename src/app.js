import { createSSRApp, createApp } from 'vue';
import App from './App.vue';

import initStore from './store';
import initRouter from './router';

import initHttp from './api/http'
import initApi from './api'
import initRequestHandler from './common/requestHandler'
import initResponseHandler from './common/responseHandler'

const STATE = 'INITIAL_STATE'

export default async () => {

	const isServer = process.isServer

	const http = initHttp()

	const apiModules = initApi(http)

	const store = initStore({
		modules: {
			productsAPI: apiModules.products,
			cartAPI: apiModules.cart,
			authAPI: apiModules.auth
		},
		isServer
	})

	const router = initRouter(store)

	initRequestHandler(http, !isServer)

	initResponseHandler(store, http, router, !isServer)

	await store.dispatch('products/load')

	if(!isServer){
		store.dispatch('user/autoLogin')
		store.dispatch('cart/load')
	}

	let app

	if(isSSRPage(router)){
		app = createSSRApp(App)
	}else{
		app = createApp(App)
	}

	app.config.globalProperties.$api = apiModules

	app.use(store).use(router);

	router.currentRoute._value.matched.forEach(route => {
		console.log(route)
	})

	return { app, router, store };
}

const isSSRPage = () => {
	return !process.isServer && Object(window).hasOwnProperty(STATE)
}

