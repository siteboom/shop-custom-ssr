import initState from './state';
import initGetters from './getters';
import initMutations from './mutations';
import initActions from './actions';

export default ({cartAPI}) => ({
	namespaced: true,
	state: initState(),
	getters: initGetters(),
	mutations: initMutations(),
	actions: initActions({cartAPI})
})