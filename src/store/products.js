export default ({productsAPI}) => ({
	namespaced: true,
	state: {
		items: null
	},
	getters: {
		all: state => state.items,
		one: state => id => state.items.find(pr => pr.id == id),
		/* one(state){
			return function(id){
				return state.items.find(pr => pr.id == id);
			}
		}*/
	},
	mutations: {
		setItems(state, items){
			state.items = items;
		}
	},
	actions: {
		async load({ commit, state }){
			let products = await productsAPI.all();

			commit('setItems', products);

			return products;
		}
	}
})