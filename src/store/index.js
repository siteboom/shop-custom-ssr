import { createStore } from 'vuex';

import initCart from './cart';
import initProducts from './products';
import initUser from './user';
import initAlerts from './alerts';

export default ({modules: {productsAPI, cartAPI, authAPI}}) => {
	return createStore({
		modules: {
			products: initProducts({productsAPI}),
			user: initUser({authAPI}),
			alerts: initAlerts(),
			cart: initCart({cartAPI}),
		},
		strict: process.env.NODE_ENV !== 'production'
	});
};