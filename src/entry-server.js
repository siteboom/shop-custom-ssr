import initApp from './app'

export default async ({url}) => {
    const {app, store, router}  = await initApp()

    await router.push(url)

    router.currentRoute.value.matched.forEach(route => {
        console.log(route)
    })

    return {app, store, router}
}