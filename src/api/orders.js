import initHttp from '@/api/http';

export async function all(){
	const http = initHttp()

	let { data } = await http.get('orders.php', {
		errorAlert: 'при получении списка заказов'
	});
	return data;
}