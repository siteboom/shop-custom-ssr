import initProductsAPI from "./products"
import initCartAPI from "./cart"
import initAuthAPI from "./auth"

export default (http) => ({
    products: initProductsAPI(http),
    cart: initCartAPI(http),
    auth: initAuthAPI(http),
})