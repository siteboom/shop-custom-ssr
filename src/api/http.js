import axios from 'axios';
import {getAccessToken} from "../utils/tokens";

export function addAccessToken(request){
	let token = getAccessToken();

	if(token !== null){
		request.headers.Authorization = `Bearer ${token}`;
	}

	return request;
}

export default () => {
	const baseURL = 'https://wp.dmitrylavrik.ru/vue-advanced-api-l3/';
	const instance = axios.create({
		baseURL,
		timeout: 10000,
		withCredentials: true
	});

	return instance
}