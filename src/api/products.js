export default (http) => ({
	all: async () => {
		let { data } = await http.get('products.php');
		return data;
	},
	rating: async (id) => {
		let { data } = await http.get('ratings.php', {
			params: { id },
			errorAlert: { text: 'при получении рейтинга товара' }
		});
		return data;
	},
	mark: async (id, mark) => {
		let { data } = await http.put('ratings.php', { id, mark }, {
			errorAlert: { text: 'при оценке товара' }
		});
		return data;
	}
})