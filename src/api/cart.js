export default (http) => ({
	load: async (token) => {
		let { data } = await http.get('cart.php', {
			params: { token },
			errorAlert: {
				text: 'при загрузке корзины',
				critical: true
			}
		});

		return data;
	},
	add: async (token, id) => {
		let { data } = await http.post('cart.php', { token, id }, {
			errorAlert: 'при добавлении товара'
		});
		return data;
	},

	remove: async (token, id) => {
		let { data } = await http.delete('cart.php', {
			params: { token, id },
			errorAlert: 'при удалении товара'
		});
		return data;
	},

	change: async (token, id, cnt) => {
		let { data } = await http.put('cart.php', { token, id, cnt }, {
			errorAlert: 'при изменении количества товара'
		});
		return data;
	}
})