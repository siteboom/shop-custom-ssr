import {addAccessToken} from "../api/http";

export default (instance, isClient) => {
    if(isClient){
        instance.interceptors.request.use(addAccessToken);
    }
}