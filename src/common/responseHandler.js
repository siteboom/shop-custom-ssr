import axios from "axios";
import {cleanTokensData, setTokens} from "../utils/tokens";
import {addAccessToken} from "../api/http";

function addResponseHandler(instance, success, error){
    instance.interceptors.response.use(success, error);
}

export default (store, instanceHttp, router, isClient) => {
    instanceHttp.interceptors.response.use(
        r => r,
        async error => {
            if(error.response.status !== 401){
                return Promise.reject(error); // ошибка, не связанная с авторизацией
            }

            if(isClient){
                cleanTokensData();
                let response = await instanceHttp.get('auth/refresh/refresh.php');

                if(!response.data.res){
                    return Promise.reject(error); // прокидываем 401 код дальше
                }

                setTokens(response.data.accessToken);
                return axios(addAccessToken(error.config));
            }
        }
    );

    addResponseHandler(
        instanceHttp,
        function(response){
            if('errorAlert' in response.config){
                response.data = { res: true, data: response.data };
            }

            return response;
        },
        function(error){
            console.log(error, 'err')
            let config = error.response.config;

            if(isClient && error.response.status == 401 && config.appSilence401 !== true){
                // clean user data & tokens
                store.dispatch('user/clean');

                router.push({name: 'login'}).then(() => {
                    location.reload(); // options vs 33 idea
                });
                return; // ?
            }

            if('errorAlert' in config){
                let { errorAlert } = config;

                if(typeof errorAlert === 'string'){
                    errorAlert = { text: errorAlert };
                }

                store.dispatch('alerts/add', {
                    text: 'Ошибка ответа от сервера ' + errorAlert.text,
                    timeout: errorAlert.timeout ?? 5000,
                    critical: errorAlert.critical ?? false
                });

                return { data: { res: false, data: null }};
            }

            return Promise.reject(error);
        }
    );
}

