const isSSRPage = (router) => {
    return !router.currentRoute.value.matched.some(record => record.meta.noSSR || record.meta.auth)
}

module.exports = {isSSRPage}